import React, { useState } from 'react'
import {useParams } from 'react-router-dom';
import '../components/statics/CSS/InfoTech.css'
import { Footer } from '../components/Footer';

function InfoParte() {

    const params = useParams();

    const [parte]=useState([
        {
            id: 1,
            title: "Disco Music",
            img: "https://img.freepik.com/vector-gratis/fondo-bola-discoteca_1284-5130.jpg?w=740&t=st=1670264125~exp=1670264725~hmac=4fc258e4ef036f125e5e3cc7d55af1d46aae1eae4a01eb8ff9f495c05aa1c028",
            opn: "Marck se encontraba escuchando la musica de la capitana y manifestaba que no era de su agrado, ya que era musica de disco; se concidera un poco irrelevante",
        },
        {
            id: 2,
            title: "Botánicos",
            img: "https://img.freepik.com/vector-gratis/conjunto-hojas_53876-91268.jpg?w=1060&t=st=1670264270~exp=1670264870~hmac=f30a9ee5fd26dc34a46058bb3e7ee7d15c95e8187b59d94b3a742349a1e2c0a5",
            opn: "La comunicacion de Marck con otros botanicos, quienes reconocieron que realizo un buen trabajo con la plantacion de patatas, ya que son personajes que no tienen mucha relevancio dentro del contenido",
        },
        {
            id: 3,
            title: "Purificador",
            img: "https://cdn-icons-png.flaticon.com/512/1138/1138977.png?w=740&t=st=1670264389~exp=1670264989~hmac=786e94d457caf509f8d774ef76bb75ec235fcff0ca19f9fbbaf1755871c9d96e",
            opn: "La conversación con la NASA acerca del purificador de agua, ya que en varias oportunidades se había repetido la importancia de este elemento.",
        },
    ]);

    let selectedItem = parte.find(item => item.id === parseInt(params.id));

    return (
    <React.Fragment>
                <main>
                    <header><h3>{selectedItem.title}</h3></header>
                    <article>
                    <img className='izqT' src={selectedItem.img} alt="Imagen Tech"/>
                        <section className="Cuerpo">
                            <h4>Opinión</h4>
                            <p>{selectedItem.opn}</p>
                        </section>
                    </article>
                </main>
        <Footer/>
    </React.Fragment>
    );
}

export {InfoParte};