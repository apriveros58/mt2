import React from 'react'
import '../components/statics/CSS/LogIn.css'
import IconButton from '@mui/material/IconButton';
import PersonOutlineOutlinedIcon from '@mui/icons-material/PersonOutlineOutlined';
import HttpsOutlinedIcon from '@mui/icons-material/HttpsOutlined';
import LoginOutlinedIcon from '@mui/icons-material/LoginOutlined';
import {
    Link
    } from "react-router-dom";

function LogIn() {
    return (
    <React.Fragment>
        <section className='LogIn'>
            <Link to="/"><header><label><h1>Medias Toronjas</h1></label></header></Link>
            <section className='centro'>
                <img src="https://9n52lh.deta.dev/download/Logo.png" alt ="Logo"/>
                <form>
                    <ul>
                        <li>
                            <PersonOutlineOutlinedIcon/>
                            <input type="email" name="login_mail" id="login_mail" placeholder="Correo electrónico" required disabled ></input>
                        </li>
                        <li>
                            <HttpsOutlinedIcon/>
                            <input type="password" name="login_pass" id="login_pass" placeholder="Constraseña" required disabled ></input>
                        </li>
                    </ul>
                    <IconButton aria-label="Iniciar Sesión">
                        <Link to="/Admin"><LoginOutlinedIcon/></Link>
                    </IconButton>
                </form>
            </section>
        </section>
    </React.Fragment>
    );
}

export {LogIn};