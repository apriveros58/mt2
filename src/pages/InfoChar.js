import React, { useState } from 'react'
import {useParams } from 'react-router-dom';
import '../components/statics/CSS/InfoTech.css'
import { Footer } from '../components/Footer';

function InfoChar() {

    const params = useParams();

    const [people]=useState([
        {
            id: 1,
            nom: "Capitana Lewis",
            img: "https://expedientmeans.files.wordpress.com/2015/10/jessica-chastain-as-commander-lewis-the-martian.jpg",
            opn: "El personaje que leí con más agrado fue el de la Capitana Lewis, esto porque es una mujer brillante, con capacidades de liderazgo y alto grado de simpatía por sus compañeros. La capitana Lewis es un claro ejemplo de la forma en la cual un líder debe dirigir a su equipo, tomando decisiones con determinación pero teniendo en cuenta a su equipo en este proceso en todo momento, adicionalmente, no se enfoca solo en la parte técnica o de operación, también muestra gran interés por el bienestar de sus compañeros de tripulación, tanto así, que prefiere arriesgarse ella como líder, en lugar de arriesgar la vida de uno de sus astronautas",
        },
        {
            id: 2,
            nom: "Rick Martínez",
            img: "https://static.wikia.nocookie.net/the-martian/images/a/a5/Martinez_1.jpg",
            opn: "Rick es un astronauta de la mision Ares 3, que salio junto a Marck de la tierra, especialista en Vuelo espacial y piloto de la mision. Bajo mi opinion es el mejor astronauta de su mision, pues fue pieza fundamental en que se cumpliera la mision ares 3 con sus grandes habilidades (tanto asi que Mark cada que puede lo elogia), asi como un gran amigo, pues siento el soporte de su familia y teniendo un hijo pequeño nunca dudo en volver para rescatar a Mark, incluso con humor le comunica a su familia la noticia y se convierte en el sosten psicologico de sus compañeros por su buen trato. Ademas, es latinoamericano."
        },
        {
            id: 3,
            nom: "Venkat / Vincent Kapoor",
            img: "https://pbs.twimg.com/media/EDonrkrVUAEce6X?format=jpg&name=large",
            opn: "Es un personaje que apesar de encontrarse en situaciones complejas o de mucha tensión, siempre trata de ver el lado positivo o las probabilidades más favorables de la situación. En ocasiones puede leerse que sus ideales entran en conflicto que su superior, el director de la NASA, pero no deja de percibirse humano, entiende que más allá de los problemas técnicos y económicos, lo importante es la vida y el bienestar de todos los integrantes, tanto los que están en el viaje de regreso, el que está solo en un planeta extraño, y los que se encuentran en la Tierra "
        },
        {
            id: 4,
            nom: "Teddy Sanders",
            img: "https://assets.mycast.io/characters/teddy-sanders-1191753-normal.jpg?1605550892",
            opn: "El personaje con el que menos empatía surgió al momento de realizar la lectura fue con Teddy Sanders, Teddy es el director de la NASA y esta más enfocado a otro tipo de asuntos que el considera de mayor importancia, por lo que a su parecer, la vida de Marck es un asunto no prioritario, adicionalmente, es un personaje con un poco de frialdad en su personalidad"
        },
        {
            id: 5,
            nom: "Rich Purnell",
            img: "https://static.wikia.nocookie.net/the-martian/images/1/19/Rich_Purnell_1.jpg",
            opn: "Rich es un cientifico que trabaja para la NASA, es quien tiene la idea de volver por Mark sin que la tripulacion del ares 3 descienda en la tierra y como enviar suministros. No me gusta la actitud despota y sobrada que tiene la persona al momento de transmitir las ideas al publico, pues ante todo uno debe ponerse al nivel del receptor para poder transmitir ideas de manera optima y mas en momentos decisivos como ese. Asi mismo, no tiene respeto por sus superiores, por sus colegas y segun el desorden de su puesto de trabajo, por nadie"
        },
        {
            id: 6,
            nom: "Mark Watney",
            img: "https://static.wikia.nocookie.net/the-martian/images/1/18/Mark_Watney.jpg",
            opn: "No es que me caiga mal del todo, de hecho tiene un muy buen sentido del humor, considerando la situación en la que se encuentra; sino que en ocasiones se percibe como un robot, realizando tareas constantemente y siendo muy técnico, es un poco grosero o altanero en algunas partes, esto se acepta debido a que debe trabajar con urgencia para no morir, pero aún así en ocasiones es tedioso leer sus partes, por otro lado, puede llegar a ser muy sobrado o arrogante, especialmente cuando después de hacer contacto con Tierra, pues da la sensación de que al sobrevivir todo esto y llegar como un héroe a la tierra, a veces tiende a mostrarse con cierta superioridad"
        },
    ]);

    let selectedItem = people.find(item => item.id === parseInt(params.id));

    return (
    <React.Fragment>
        
                <main>
                    <header><h3>{selectedItem.nom}</h3></header>
                    <article>
                        <img className='izqT' src={selectedItem.img} alt="Personaje"/>
                        <section className="Cuerpo">
                            <h4>Opinión</h4>
                            <p>{selectedItem.opn}</p>
                        </section>
                    </article>
                </main>
        <Footer/>
    </React.Fragment>
    );
}

export {InfoChar};