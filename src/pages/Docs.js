import React from 'react'
import '../components/statics/CSS/Docs.css'
import { Footer } from '../components/Footer';
import { Link } from "react-router-dom"


function Docs() {
    return (
    <React.Fragment>
        <section className='Docs'>
            <header>
                <nav className='Principal'>
                    <ul>
                        <li><Link to="/" title='Home'><img className="img_logo" src="https://9n52lh.deta.dev/download/Logo.png" alt="Logo"/></Link></li>
                        <li><ul>
                            <li><a href='#Overview'><h5>Overview</h5></a></li>
                            <li><a href='#Requerimientos'><h5>Requerimientos</h5></a></li>
                            <li><a href='#Arquitectura'><h5>Arquitectura</h5></a></li>
                            <li><a href='#Limitaciones'><h5>Limitaciones</h5></a></li>
                            <li><a href='#Costos'><h5>Costos</h5></a></li>
                        </ul></li>
                    </ul>
                    
                    
                </nav>
            </header>
            <article>
                <h4>Documentación</h4>
                <div id='Overview'>
                    <h3>Overview</h3>
                    <ul>
                        <li>
                            <h6>Problema a resolver</h6>
                            <p>En la actualidad la mayoría de pequeñas empresas colombianas no cuentan con una plataforma virtual donde puedan darse a conocer, tener contacto directo con sus clientes y comercializar sus productos; y en dado caso de poseerlas usualmente son páginas estáticas, con información desactualizada, sin posibilidades de cerrar ventas en línea, y pocas veces tienen conexión con sus invetarios. Para los pequeños empresarios esto representa poca visibilidad virtual, y pese a que algunos cuentan con puntos de venta directo exitosos, como el de <strong>Medias Toronjas</strong>, en los últimos años se ha incrementado la demanda de realizar ventas por internet, por lo que se hace necesario migrar los negocios a un entorno virtual de forma óptima, eficaz e interconectada, para que de esta manera puedan alcanzar nuevos públicos, conservar sus clientes y mejorar la operación interna.</p>
                        </li>
                        <li>
                            <h6>Alcance</h6>
                            <p>En esta parte incial de 16 semanas se desea crear el prototipo del funcionamiento de la página web de <strong>Medias Toronjas</strong> para empezar con su migración a un entorno virtual con el fin de atraer nuevos clientes, mantener los existentes y mejorar los sistemas de inventario internos.</p>
                        </li>
                        <li>
                            <h6>Recursos</h6>
                            <ul className='recursos'>
                                <li>
                                    <h7>Humanos</h7><br></br>
                                    <img className='imgDocs' src="https://9n52lh.deta.dev/download/RH.png" alt="Recursos Humanos"/>
                                </li>
                                <li>
                                    <h7>Software/Materiales</h7><br></br>
                                    <img className='imgDocs' src="https://9n52lh.deta.dev/download/RM.png" alt="Recursos Materiales/Software"/>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <h6>Entregables</h6>
                            <ul className='entregables'>
                                <li>Prototipo funcional de la capa de Administración de Productos</li>
                                <li>Estructura página HOME</li>
                            </ul>
                        </li>
                        <li>
                            <h6>Diagrama de Gantt</h6>
                            <ul className='recursos'>
                                <li>
                                    <h7>Vista Global</h7><br></br>
                                    <img className='imgDocs' src="https://9n52lh.deta.dev/download/vgGantt.png" alt="Gantt Global"/>
                                </li>
                                <li>
                                    <h7>Vista específica</h7><br></br>
                                    <img className='imgDocs' src="https://9n52lh.deta.dev/download/veGantt.png" alt="Gantt Específico"/>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div id='Requerimientos'>
                    <h3>Requerimientos</h3>
                    <img className='imgDocs' src="https://9n52lh.deta.dev/download/Requerimientos.png" alt="Requerimientos"/>
                    <h6>Historias de usuario</h6>
                    <ul className='recursos'>
                        <li>
                            <h7>RF01 Login Administrador</h7><br></br>
                            <img className='imgDocs' src="https://9n52lh.deta.dev/download/R1.png" alt="RF01"/>
                        </li>
                        <li>
                            <h7>RF02 Agregar Producto</h7><br></br>
                            <img className='imgDocs' src="https://9n52lh.deta.dev/download/R2.png" alt="RF02"/>
                        </li>
                        <li>
                            <h7>RF03 Modificar Producto</h7><br></br>
                            <img className='imgDocs' src="https://9n52lh.deta.dev/download/R3.png" alt="RF03"/>
                        </li>
                        <li>
                            <h7>RF04 Eliminar Producto</h7><br></br>
                            <img className='imgDocs' src="https://9n52lh.deta.dev/download/R4.png" alt="RF04"/>
                        </li>
                        <li>
                            <h7>RF05 Ordenar Producto</h7><br></br>
                            <img className='imgDocs' src="https://9n52lh.deta.dev/download/R5.png" alt="RF05"/>
                        </li>
                        <li>
                            <h7>RF06 Filtrar Producto</h7><br></br>
                            <img className='imgDocs' src="https://9n52lh.deta.dev/download/R6.png" alt="RF06"/>
                        </li>
                        <li>
                            <h7>RF07 Buscar Producto</h7><br></br>
                            <img className='imgDocs' src="https://9n52lh.deta.dev/download/R7.png" alt="RF07"/>
                        </li>
                        <li>
                            <h7>RF08 Cerrar Sesión</h7><br></br>
                            <img className='imgDocs' src="https://9n52lh.deta.dev/download/R8.png" alt="RF08"/>
                        </li>
                    </ul>
                    <h6>Out of Scop</h6>
                    <ul className='entregables'>
                        <li>RF01 Login Administrador</li>
                        <li>RF06 Filtrar Productos</li>
                        <li>RF07 Buscar Productos</li>
                        <li>RF08 Cerrar Sesión</li>
                    </ul>
                </div>
                <div id='Arquitectura'>
                    <h3>Arquitectura</h3>
                    <h6>Diagramas</h6>
                    <ul className='recursos'>
                        <li>
                            <h7>Diagrama de Contexto</h7><br></br>
                            <img className='imgDocs' src="https://9n52lh.deta.dev/download/R1.png" alt="RF01"/>
                        </li>
                        <li>
                            <h7>Diagrama de Componentes</h7><br></br>
                            <img className='imgDocs' src="https://9n52lh.deta.dev/download/R2.png" alt="RF02"/>
                        </li>
                        <li>
                            <h7>Diagrama de Secuencia</h7><br></br>
                            <img className='imgDocs' src="https://9n52lh.deta.dev/download/R3.png" alt="RF03"/>
                        </li>
                        <li>
                            <h7>Diagrama de Procesos</h7><br></br>
                            <img className='imgDocs' src="https://9n52lh.deta.dev/download/R4.png" alt="RF04"/>
                        </li>
                    </ul>
                    <h6>Modelo de Datos</h6>
                    <ul className='recursos'>
                        <li>
                            <h7>Modelo Entidad-Relación</h7><br></br>
                            <img className='imgDocs' src="https://9n52lh.deta.dev/download/MER.png" alt="Modelo Entidad-Relación"/>
                        </li>
                        <li>
                            <h7>Mapa de EndPoints</h7><br></br>
                            <img className='imgDocs' src="https://9n52lh.deta.dev/download/ME.png" alt="Mapa de EndPoints"/>
                        </li>
                    </ul>
                </div>
                <div id='Limitaciones'>
                    <h3>Limitaciones</h3>
                    <p>Dado el alcance del proyecto y su ejecución, la principal limitante fue el grado de conocimientos de los autores del proyecto. El uso de herramientas nuevas, el desarrollo de habilidades y la ejecución de acciones antes desconocidas se presentaron como el mayor factor de dificultad a la hora de codificar y ejecutar el proyecto. Ademas, el tiempo limitado generó prisas en la hora de adquirir conocimiento y ejecutarlo, generando diversos formatos de codigo a presentar, pues si bien la teoria era clara, los dos enfoques exigidos por parte de los calificadores causo que el codigo se desviara de las lecciones recibidas, dificultando su finalización y optimo despliegue. Generalizando, las limitantes percibidas para la entrega y ejecución del presente proyecto se concretan en :</p>
                    <ul className='entregables'>
                        <li>Cortos tiempos de codificación y entrega</li>
                        <li>Diversidad en items a calificar</li>
                        <li>Expertis de los autores</li>
                    </ul>
                </div>
                <div id='Costos'>
                    <h3>Costos</h3>
                    <ul className='recursos'>
                        <li>
                            <h7>Humanos</h7><br></br>
                            <img className='imgDocs' src="https://9n52lh.deta.dev/download/CH.png" alt="Recursos Humanos"/>
                        </li>
                        <li>
                            <h7>Software/Materiales</h7><br></br>
                            <ul>
                                <li>
                                    <p><strong>Base de Datos</strong><br></br>Tomando la versión Standard Basic de Heroku que posee 64Gb de memoria, 120 conecciones, sin límite de filas y una memoria RAM de 4Gb, con revisiones constantes de seguridad y funcionamiento, backups diarios y accesso SSL protegido se pagaría <strong>$50 USD/mes</strong></p>
                                </li>
                                <li>
                                    <p><strong>Servidor Deta</strong><br></br>Deta permite desplegar y mantener la página web, así como tener un servidor de imágenes, 5Gb de memoria por 100 mil consultas al mes, dominios personalizados; se pagaría  <strong>$15 USD/mes</strong></p>
                                </li>
                            </ul>
                            <h6>Total al mes en USD: <strong>$ 488.92</strong></h6>
                            <h6>Total al mes en COP: <strong>$ 2'331.204.78</strong></h6>
                        </li>
                    </ul>
                </div>
            </article>
        </section>
        <Footer/>
    </React.Fragment>
    );
}

export {Docs};