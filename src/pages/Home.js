import React from 'react'
import { HeaderIndex } from '../components/HeaderIndex';
import { GrillaProd } from '../components/GrillaProd';
import { SearchBar } from '../components/SearchBar';
import { Slider } from '../components/Slider'
import { Footer } from '../components/Footer';

function Home() {
    return (
    <React.Fragment>
        <HeaderIndex/>
        <SearchBar/>
        <Slider/>
        <GrillaProd/>
        <Footer/>
    </React.Fragment>
    );
}

export {Home};