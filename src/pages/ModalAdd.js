import React from 'react'
import { AddProd } from '../components/AddProd';
import { CabeceraModal } from '../components/CabeceraModal';
import '../components/statics/CSS/Modal.css'
import {
    Link
    } from "react-router-dom";

function ModalAdd() {
    return (
    <React.Fragment>
        <article className='Modal'>
            <section className='Contenedor'>
            <Link to="/Admin"><CabeceraModal /></Link>
                <AddProd/>
            </section>
        </article>
    </React.Fragment>
    );
}

export {ModalAdd};