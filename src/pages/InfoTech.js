import React, { useState } from 'react'
import {useParams } from 'react-router-dom';
import '../components/statics/CSS/InfoTech.css'
import { Footer } from '../components/Footer';

function InfoTech() {

    const params = useParams();

    const [tech]=useState([
        {
            id: 1,
            nom: "HAB",
            img: "https://9n52lh.deta.dev/download/hab.png",
            des: "El Hab contaba con un diseño robusto y resistente, era usado como resguardo de los astronautas y fue construido para una duracion de 30 dias, el hab contaba con suministros medicos, lugares para dormir, entre otros; ademas, almacenaba todas las tecnologias y elementos usados durante la mision, como las comunicaciopones con el VAM, el oxigenador y el purificador de agua. Enla historia, fue acogido por toda la tripulación, pero fue usado principalmente por Mark Watney,cuando el resto de la tripulación partió del planeta Marte. Ha estado presente a lo largo de la historia, ya que es el lugar en el cual habita el personaje principal. La NASA cuenta con HERA, el cual es un modulo para entrenamientos desde 14 hasta 60 días, Hera cuenta con 2 pisos, el los cuales existen cuartos para dormir, para trabajar, un modulo de higiene, red de monitoreo y capacidad para que los astronautas puedan alimentarse 3 veces al día. Para mayor detalle por favor visualizar el instructivo https://www.nasa.gov/sites/default/files/files/HRP-HERA-Experiment-Information-Package.pdf"
        },
        {
            id: 2,
            nom: "Purificador de agua",
            img: "https://9n52lh.deta.dev/download/purificador.png",
            des: "Este elemento fue categorizado como la mejor tecnologia disponible en la tierra y es escencial para la supervivencia de los astronautas, estaba diseñado para purificar orina y humedad del aire; es utilizado por Mark Watney y es mencionado desde el sol 6 hasta el 388, por mas de 30 veces. La NASA cuenta con el El Water Processor Assembly (WPA) para uso en la Estación Espacial Internacional (ISS), es un sistema que permite filtrar orina, sudor y aire generado por los astronautas i convertirlos en agua potable, tambien permite realizar otros tratamientos a este liquido, este sistema debe reemplazarse cada 30 días y no filtra algunos contaminantes; se ha pensado usarlo para beneficio de toda la comunidad"
        },
        {
            id: 3,
            nom: "ROVER",
            img: "https://9n52lh.deta.dev/download/Rover.png",
            des: "El rover es el vehículo de superficie robótico Sojourner, el cual posee una batería es de litio cloruro de tionilo, no recargable, pero que puede ser recargada por medio de paneles solares, ha sido usado por Mark Watney, quien logra usarlo en el sol 71 para llegar hasta la Pathfinder y tratar de establecer comunicación con la tierra. El vehículo de exploración espacial de la NASA (SEV),se encuentra en pruebas con el fin de que loa astronautas puedan viajar a marte o a cualquier planeta, tiene la cabina montada sobre un chasis, con ruedas que pueden girar 360 grados y conducir a unos 10 kilómetros por hora en cualquier dirección. Tiene aproximadamente el tamaño de una camioneta (con 12 ruedas) y puede albergar a dos astronautas por hasta 14 días con dormitorios e instalaciones sanitarias"
        },
        {
            id: 4,
            nom: "RTG",
            img: "https://9n52lh.deta.dev/download/RTG.png",
            des: "El dispositivo almacena plutonio, capta la radiación en forma de calor y la convierte en electricidad, fue usada para calentar el vehículo de superficie y posteriormente usado para distribuir esta electricidad uniformemente; además, es altamente radioactivo, Mark Watney lo usa en el sol 68, cuando a él se le ocurrió la idea de desenterrarlo del lugar en el que Lewis lo había guardado. Los generadores termoeléctricos de radioisótopos (RTG) son sistemas de energía de naves espaciales; es usada por la NASA donde opciones como la energía solar, no son viables o no dan a basto para la misión. Más de dos docenas de misiones espaciales estadounidenses han utilizado RTG desde que se lanzó la primera en 1961."
        },
        {
            id: 5,
            nom: "Propulsor iónico",
            img: "https://9n52lh.deta.dev/download/propulsor.png",
            des: "Los iones se usaron en el motor de La Hermes con el fin de conseguir una aceleración constante, a pesar de que es lenta, es la unica forma de obtener la constancia necesaria para llegar a Marte. Fueron usados para Los motores de la nave Hemes, Fue mencionada desde el sol 6. La NASA ha usado los iones durante varias decadas con el fin de generar propulsión (NEXT-C), tienen una mejor eficacia de combustible frente a los motores liquidos, son usados en misiones como la Deep Space"
        },
    ]);

    let selectedItem = tech.find(item => item.id === parseInt(params.id));

    return (
    <React.Fragment>
        <main>
            <header><h3>{selectedItem.nom}</h3></header>
            <article>
                <img className='izq' src={selectedItem.img} alt="Imagen Tech"/>
                <section className="Cuerpo">
                    <h4>Descripción</h4>
                    <p>{selectedItem.des}</p>
                </section>
            </article>
        </main>
        <Footer/>
    </React.Fragment>
    );
}

export {InfoTech};
