import React from 'react'
import { AdmnProd } from '../components/AdmnProd';
import { CabeceraModal } from '../components/CabeceraModal';
import '../components/statics/CSS/Modal.css'
import {
    Link
    } from "react-router-dom";

function ModalAdmn() {
    return (
    <React.Fragment>
        <article className='Modal'>
            <section className='Contenedor'>
            <Link to="/Admin"><CabeceraModal /></Link>
                <AdmnProd/>
            </section>
        </article>
    </React.Fragment>
    );
}

export {ModalAdmn};