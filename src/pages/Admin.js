import React from 'react'
import { HeaderAdmin } from '../components/HeaderAdmin';
import { SearchBar } from '../components/SearchBar';
import { AdminBar } from '../components/AdminBar';
import { GrillaProdAdm } from '../components/GrillaProdAdm';
import { Filtros } from '../components/Filtros';

function Admin() {
    return (
    <React.Fragment>
        <HeaderAdmin/>
        <SearchBar/>
        <AdminBar/>
        <Filtros/>
        <GrillaProdAdm/>
    </React.Fragment>
    );
}

export {Admin};