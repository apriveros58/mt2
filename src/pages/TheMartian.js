import React from 'react'
import '../components/statics/CSS/TheMartian.css'
import { Footer } from '../components/Footer';
import { HeaderMars } from '../components/HeaderMars';
import { GenInfoMars } from '../components/GenInfoMars';

function TheMartian() {
    return (
    <React.Fragment>
        <HeaderMars/>
        <GenInfoMars/>
        <Footer/>
    </React.Fragment>
    );
}

export {TheMartian};