import React from 'react'
import { CabeceraModal } from '../components/CabeceraModal';
import { ContenidoModalIndex } from '../components/ContenidoModalIndex';
import '../components/statics/CSS/Modal.css'
import {
    Link
    } from "react-router-dom";

function ModalIndex() {
    return (
    <React.Fragment>
        <article className='Modal'>
            <section className='Contenedor'>
            <Link to="/"><CabeceraModal /></Link>
                <ContenidoModalIndex/>
            </section>
        </article>
    </React.Fragment>
    );
}

export {ModalIndex};