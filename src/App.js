import React from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  } from "react-router-dom";
  import {LogIn} from './pages/LogIn';
  import {Admin} from './pages/Admin';
  import {Home} from './pages/Home';
  import {ModalAdd} from './pages/ModalAdd';
  import {ModalAdmn} from './pages/ModalAdmn';
  import {ModalIndex} from './pages/ModalIndex';
  import {TheMartian} from './pages/TheMartian';
  import {Docs} from './pages/Docs';
  import {InfoTech} from './pages/InfoTech';
  import {InfoChar} from './pages/InfoChar';
  import {InfoParte} from './pages/InfoParte';

  function App() {
    return(
      
      <Router>
        <Navegation/>
        <Routes>
        <Route path='/' element={<Home/>}></Route>
          <Route path='/LogIn' element={<LogIn/>}></Route>
          <Route path='/Admin' element={<Admin/>}></Route>
          <Route path='/InfoProduct/:id' element={<ModalIndex/>}></Route>
          <Route path='/Edit/:id' element={<ModalAdmn/>}></Route>
          <Route path='/Add' element={<ModalAdd/>}></Route>
          <Route path='/TheMartian' element={<TheMartian/>}></Route>
          <Route path='/Docs' element={<Docs/>}></Route>
          <Route path='/InfoTech/:id' element={<InfoTech/>}></Route>
          <Route path='/InfoChar/:id' element={<InfoChar/>}></Route>
          <Route path='/InfoParte/:id' element={<InfoParte/>}></Route>
        </Routes>
        </Router>
     );

  }

  function Navegation(){
    return
  }

/*import { Switch } from '@mui/material';*/

/* ----- Para ir a otra página ----- */





/*

    <React.Fragment>
      <Admin/>
      <Home/>
      {/*
      <Home/>
      <LogIn/>
       }*/
    /*</React.Fragment>
  );
}*/

export default App;
