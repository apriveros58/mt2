import React from 'react'
/* Para importar los estilos CSS */
import './statics/CSS/GrillaTech.css'
/*Para organizar grilla */
import { InfoCard } from './InfoCard'
import {
    Link
    } from "react-router-dom";

const GrillaCharLoved = () => {
    return (
        <React.Fragment>
            <div className='GrillaTech'>
                <Link to="/InfoChar/1">
                    <button>
                    <InfoCard
                        name="Capitana Lewis"
                        img="https://static.wikia.nocookie.net/the-martian/images/0/0b/1_Lewis.jpg"/>
                    </button>
                </Link>
                <Link to="/InfoChar/2">
                    <button>
                    <InfoCard
                        name="Rick Martinez"
                        img="https://static.wikia.nocookie.net/the-martian/images/a/a5/Martinez_1.jpg"/>
                    </button>
                </Link>
                <Link to="/InfoChar/3">
                    <button>
                    <InfoCard
                        name="Venkat Kapoor"
                        img="https://pbs.twimg.com/media/EDonrkrVUAEce6X?format=jpg&name=large"/>
                    </button>
                </Link>
            </div>
        </React.Fragment>

    )

}
export {GrillaCharLoved}