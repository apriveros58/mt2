const API_URL = "https://reactdjangoplancr.herokuapp.com/tienda/product/";
const API_URLM = "https://reactdjangoplancr.herokuapp.com/tienda/material/";
const API_URLH = "https://reactdjangoplancr.herokuapp.com/tienda/hormas/";

export const listProducts = async () => {
    return await fetch(API_URL);
};

export const getProducts = async (productId) => {
    return await fetch(`${API_URL}${productId}`);
};


export const registerProducts = async (newProduct) => {
    return await fetch(API_URL,{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "name": String(newProduct.name).trim(),
            "id_horma_id": parseInt(newProduct.id_horma_id),
            "id_material_id": parseInt(newProduct.id_material_id),
            "image": String(newProduct.image).trim(),
            "size": parseInt(newProduct.size),
            "price": String(newProduct.price).trim(),
            "stock": parseInt(newProduct.stock),
            "available": Boolean(newProduct.available),
        })
    });
};

export const updateProducts = async (productId, updateProducts) => {
    return await fetch(`${API_URL}${productId}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "name": String(updateProducts.name).trim(),
            "id_horma_id": parseInt(updateProducts.id_horma_id),
            "id_material_id": parseInt(updateProducts.id_material_id),
            "image": String(updateProducts.image).trim(),
            "size": parseInt(updateProducts.size),
            "price": String(updateProducts.price).trim(),
            "stock": parseInt(updateProducts.stock),
            "available": Boolean(updateProducts.available),
        })
    });
};

export const deleteProducts = async (productId) => {
    return await fetch(`${API_URL}${productId}`, {
        method: 'DELETE'
    });
};


export const listMaterial = async () => {
    return await fetch(API_URLM);
};

export const listHormas = async () => {
    return await fetch(API_URLH);
};