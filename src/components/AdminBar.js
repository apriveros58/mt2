import React from 'react'
import './statics/CSS/AdminBar.css'

import IconButton from '@mui/material/IconButton';
import LogoutIcon from '@mui/icons-material/Logout';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import {
    Link
    } from "react-router-dom";


function AdminBar() {
    return (
    <React.Fragment>
        <section className='AdminBar'>
            <ul>
                <li>
                    <button type='submit' title='Cerrar Sesión'>
                        <IconButton aria-label="Logout">
                            <Link to="/LogIn"><LogoutIcon/></Link>
                        </IconButton>
                    </button>
                </li>
                <li>
                    <button type='submit' title='Añadir producto'>
                            <IconButton aria-label="Add">
                                <Link to="/Add"><AddCircleIcon/></Link>
                            </IconButton>
                    </button>
                </li>
                <li>
                    <form>
                        <label class="admin_labels"><strong>Ordenar por:</strong></label>
                        <select name="Orden" disabled >
                            <option id="ordenar_item" value="1" selected disabled hidden>-- Seleccionar --</option>
                            <option id="ordenar_item" value="Nuevos primero">Nuevos primero</option>
                            <option id="ordenar_item" value="Viejos primero">Viejos primero</option>
                            <option id="ordenar_item" value="Más vendidas primero">Más vendidas primero</option>
                            <option id="ordenar_item" value="Menos vendidas primero">Menos vendidas primero</option>
                            <option id="ordenar_item" value="De menor a mayor precio ">De menor a mayor precio </option>
                            <option id="ordenar_item" value="De mayor a menor precio ">De mayor a menor precio </option>
                        </select>
                    </form>
                </li>
            </ul>
        </section>
    </React.Fragment>
    );
}

export {AdminBar};