import React from 'react'
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import { useNavigate } from 'react-router-dom';
import './statics/CSS/InfoCard.css'

const Char= (props) =>{
    const navigate=useNavigate();
    return (
        <div className='Tech'>
            <Card sx={{ maxWidth: 345 }} className="Tech" onClick={() => navigate(`/Infochar/${Char.id}`)}>
            <CardMedia
                component="img"
                height="200"
                image={props.img}
                alt={props.name}
            />
            <CardHeader
                title={props.name}
            />
        </Card>
        </div>
    );
}
export {Char};