import React from 'react'
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import IconButton from '@mui/material/IconButton';
import AddShoppingCart from '@mui/icons-material/AddShoppingCart';
import './statics/CSS/Producto.css'
import { useNavigate } from 'react-router-dom';

const Producto= ({Product}) =>{
    const navigate=useNavigate();
        return (
            <React.Fragment>
                <Card sx={{ maxWidth: 345 }} className="Producto" onClick={() => navigate(`/InfoProduct/${Product.id}`)}>
                    <CardMedia
                        component="img"
                        height="200"
                        image={Product.image}
                        alt={Product.name}
                    />
                    <CardHeader
                        action={
                            <IconButton aria-label="Añadir al carrito">
                                <AddShoppingCart />
                            </IconButton>
                        }
                        title={Product.name}
                        subheader={Product.price}
                    />
                </Card>
            </React.Fragment>
        );
}
export {Producto};
