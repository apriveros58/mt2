import React, { useState } from 'react'
/* Para importar los estilos CSS */
import './statics/CSS/GrillaTech.css'
/*Para organizar grilla */
import { InfoCard } from './InfoCard'
import {
  Link
  } from "react-router-dom";

const GrillaTech = () => {
  
  return (
    <React.Fragment>
      <div className='GrillaTech'>
      <Link to="/InfoTech/1">
            <button>
              <InfoCard 
                key="1"
                name="HAB"
                img="https://upload.wikimedia.org/wikipedia/commons/d/df/Habitat_Demonstration_Unit_with_X-hab_-_D-RATS_2011.jpg"/>
            </button>
          </Link>
          <Link to="/InfoTech/2">
            <button>
              <InfoCard
                key="2"
                name="Purificador"
                img="https://media.cnnchile.com/sites/4/2020/12/aquaporin.jpg"/>
            </button>
          </Link>
          <Link to="/InfoTech/3">
            <button>
              <InfoCard
                key="3"
                name="Rover"
                img="https://cdna.artstation.com/p/assets/images/images/012/918/974/large/mario-merino-new-01-composite.jpg?1537192202"/>
            </button>
          </Link>
          <Link to="/InfoTech/4">
            <button>
              <InfoCard
                key="4"
                name="RTG"
                img="https://www.spaceflightinsider.com/wp-content/uploads/2015/07/2424-ula_atlas_v_new_horizons-carleton_bailie1.jpg"/>
            </button>
          </Link>
          <Link to="/InfoTech/5">
            <button>
              <InfoCard
                key="5"
                name="Propulsor"
                img="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Ion_Engine_Test_Firing_-_GPN-2000-000482.jpg/640px-Ion_Engine_Test_Firing_-_GPN-2000-000482.jpg"/>
            </button>
          </Link>
      </div>
  </React.Fragment>

  )

}
export {GrillaTech}

