import React from 'react'
import '../components/statics/CSS/CabeceraModal.css'


function CabeceraModal() {
    return (
    <React.Fragment>
        <header className='CabeceraModal'>
            <img src="https://9n52lh.deta.dev/download/Logo.png" alt="Logo"/>
            <button class="botones_modal" className="cerrar_modal">X</button>
        </header>
    </React.Fragment>
    );
}



export {CabeceraModal};