import React from 'react'
import './statics/CSS/HeaderMars.css'

function HeaderMars() {
    return (
        <React.Fragment>
            <header className='HeaderMars'>
                <img className="img_logoM" src="https://9n52lh.deta.dev/download/mars.png" alt="TheMartian"/>
                <h1>El Marciano</h1>
            </header>
            <nav className='Principal'>
                <ul>
                    <li><a href='#Tech' title='Tecnología'><img className="img_M" src="https://9n52lh.deta.dev/download/tech.png" alt="Tech"/></a></li>
                    <li><a href='#Loved' title='Personajes que gustaron'><img className="img_M" src="https://9n52lh.deta.dev/download/heart.png" alt="Loved"/></a></li>
                    <li><a href='#Hated' title='Personajes que NO gustaron'><img className="img_M" src="https://9n52lh.deta.dev/download/close.png" alt="Hated"/></a></li>
                    <li><a href='#Book' title='Impresiones'><img className="img_M" src="https://9n52lh.deta.dev/download/book.png" alt="Libro"/></a></li>
                </ul>
            </nav>
        </React.Fragment>
    );
}

export {HeaderMars};