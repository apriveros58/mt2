import React from 'react'
/* Para importar los estilos CSS */
import './statics/CSS/HeaderIndex.css'
/*Importar imagenes a usar*/

function HeaderIndex(){
    return (
        <React.Fragment>
            <header className='HeaderIndex' >
                <img className="img_logo" src="https://9n52lh.deta.dev/download/Logo.png" alt="Logo"/>
                <h1>Medias Toronjas</h1>
            </header>
        </React.Fragment>
    )
}

export {HeaderIndex}