import React, { useState, useEffect } from 'react'
import { useNavigate } from "react-router-dom";
import '../components/statics/CSS/ContenidoModal.css'
import * as tiendaServers from './tiendaServers'

const AddProd = () =>{

    const navigate=useNavigate();

    const initialState={
    id: 0,
    available: "true"}
    const [Products, setProducts] = useState(initialState);  

    const handleInputChange = (e) => {
        setProducts({ ...Products, [e.target.name]: e.target.value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        //console.log(Products);
        try {
            let res;
            //if (!params.id) {
            res = await tiendaServers.registerProducts(Products);
            const data = await res.json();
            console.log(data)
            /*} else {
            await tiendaServers.updateProducts(params.id, Products);
            }*/
            navigate("/Admin");
        } catch (error) {
            console.log(error);
        }
    };

    //traer informacion de materiales
    const [Materials, setMaterial] = useState([]);
    const listMaterial = async () => {
        try {
            const res = await tiendaServers.listMaterial();
            const data = await res.json();
            //console.log(data.Materials)
            setMaterial(data.Materials);
        } catch (error) {
            console.log(error);
        }
    };
    useEffect(() => {
        listMaterial();
    }, []);

    //traer informacion de Hormas
    const [Hormas, setHorma] = useState([]);
    const listHormas = async () => {
        try {
            const res = await tiendaServers.listHormas();
            const data = await res.json();
            //console.log(data.Materials)
            setHorma(data.Hormas);
        } catch (error) {
            console.log(error);
        }
    };
    useEffect(() => {
        listHormas();
    }, []);

    return (
        <React.Fragment>
            <form className='ContenidoModal' onSubmit={handleSubmit}>
                <section className="izq">
                    <table>
                        <tr>
                            <td><label for="Nombre" >Nombre: </label></td>
                            <td><input type="text" placeholder="Nombre artículo" id="nom_art2" name="name" value={Products.name} onChange={handleInputChange}/></td>
                        </tr>
                        <tr>
                            <td><label for="url" >url: </label></td>
                            <td><input type="text" placeholder="Url imagen" id="nom_art2" name="image" value={Products.image} onChange={handleInputChange}/></td>
                        </tr>
                        <tr>
                            <td><label for="Ref">Referencia: </label></td>
                            <td><label for="ref" id="ex_nom" >Ref - 0000X</label></td>
                        </tr>
                    </table>
                </section>
                <section className="der">
                    <table>
                        <tr>
                            <td><label for="Cantidad">Cantidad: </label></td>
                            <td><input name="stock" type="number" min="1" max="2500000" step="1" placeholder="Cantidad" width="25" id="cant" value={Products.stock} onChange={handleInputChange}/></td>
                        </tr>
                        <tr>
                            <td><label for="Talla">Talla: </label></td>
                            <td><input name="size" type="number" min="25" max="60" step="1" placeholder="Talla" id="talla" value={Products.size} onChange={handleInputChange}/></td>
                        </tr>
                        <tr>
                            <td><label for="Precio">Precio:</label></td>
                            <td>
                                <input name="price" type="number" min="35000" max="1000000" placeholder="$ Precio COP" id="precio" value={Products.price} onChange={handleInputChange}/>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="Modelo">Modelo: </label></td>
                            <td>
                                <select name="id_horma_id" id="modelo" value={Products.id_horma_id} onChange={handleInputChange}>
                                    <option id="ordenar_item" value="1" selected disabled hidden>-- Seleccionar --</option>
                                    {Hormas.map((Horma)=>(
                                        <option id="ordenar_item" value={Horma.id}>{Horma.name_h}</option>
                                    ))}
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="Diseño">Diseño: </label></td>
                            <td>
                                <select name="id_material_id" id="diseño" value={Products.id_material_id} onChange={handleInputChange}>
                                    <option id="ordenar_item" value="1" selected disabled hidden>-- Seleccionar --</option>
                                    {Materials.map((Material)=>(
                                        <option id="ordenar_item" value={Material.id}>{Material.name_m}</option>
                                    ))}
                                </select>
                            </td>
                        </tr>
                    </table>
                    <button className='botones_modal' id="main_button">Agregar</button>
                </section>
            </form>
        </React.Fragment>
    )
}

export {AddProd}