import React from 'react'
import './statics/CSS/HeaderAdmin.css'
import { HeaderIndex } from '../components/HeaderIndex';
import {
    Link
    } from "react-router-dom";

function HeaderAdmin() {
    return (
        <React.Fragment>
            <nav className='HeaderAdmin'>
                <Link to="/"><HeaderIndex/></Link>
            </nav>
        </React.Fragment>
    );
}

export {HeaderAdmin};