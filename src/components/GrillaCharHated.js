import React from 'react'
/* Para importar los estilos CSS */
import './statics/CSS/GrillaTech.css'
/*Para organizar grilla */
import { InfoCard } from './InfoCard'
import {
    Link
    } from "react-router-dom";

const GrillaCharHated = () => {
    return (
        <React.Fragment>
            <div className='GrillaTech'>
                <Link to="/InfoChar/4">
                    <button>
                    <InfoCard
                        name="Teddy Sanders"
                        img="https://static.wikia.nocookie.net/the-martian/images/7/76/Teddy_Sanders-0.jpg"/>
                    </button>
                </Link>
                <Link to="/InfoChar/5">
                    <button>
                    <InfoCard
                        name="Rich Punell"
                        img="https://static.wikia.nocookie.net/the-martian/images/1/19/Rich_Purnell_1.jpg"/>
                    </button>
                </Link>
                <Link to="/InfoChar/6">
                    <button>
                    <InfoCard
                        name="Mark Watney"
                        img="https://static.wikia.nocookie.net/the-martian/images/2/29/Mark_Watney_1.jpg"/>
                    </button>
                </Link>
            </div>
        </React.Fragment>

    )

}
export {GrillaCharHated}