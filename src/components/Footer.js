import React from 'react'
import './statics/CSS/Footer.css'
/*Importar imagenes a usar*/
import IconButton from '@mui/material/IconButton';
import LoginIcon from '@mui/icons-material/Login';
import HelpIcon from '@mui/icons-material/Help';
import InstagramIcon from '@mui/icons-material/Instagram';
import FacebookIcon from '@mui/icons-material/Facebook';
import CallIcon from '@mui/icons-material/Call';
import {
    Link
    } from "react-router-dom";



function Footer(){
    return (
        <React.Fragment>
            <section className='Footer'>
                <section>
                    <IconButton title='Log In'>
                        <Link to="/LogIn"><LoginIcon/></Link>
                    </IconButton>
                    <IconButton title='Home'>
                        <Link to="/"><img className="logo_marciano" src="https://9n52lh.deta.dev/download/home.png" alt="Home"/></Link>
                    </IconButton>
                    <IconButton title="El Marciano">
                        <Link to="/TheMartian"><img className="logo_marciano" src="https://9n52lh.deta.dev/download/marte.png" alt="El Marciano"/></Link>
                    </IconButton>
                    <IconButton title="Documentación">
                        <Link to="/Docs"><img className="logo_marciano" src="https://9n52lh.deta.dev/download/gitlab.png" alt="El Marciano"/></Link>
                    </IconButton>
                </section>
                <section>
                    <IconButton title='Ayuda'>
                        <HelpIcon/>
                    </IconButton>
                    <IconButton title='Instagram'>
                        <InstagramIcon/>
                    </IconButton>
                    <IconButton title='Facebook'>
                        <FacebookIcon/>
                    </IconButton>
                    <IconButton title='Contáctanos'>
                        <CallIcon/>
                    </IconButton>
                </section>
            </section>
        </React.Fragment>
    )
}

export {Footer}