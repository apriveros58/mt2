import React from 'react'
import './statics/CSS/SearchBar.css'
/*Importar imagenes a usar*/

function SearchBar(){
    return (
        <React.Fragment>
            <form className='SearchBar'>
                <input type="search" name="search"  placeholder="Buscar producto" disabled />
                <button type="submit"><img className="img_icono"src="https://9n52lh.deta.dev/download/searchIcon.png" alt="Buscar" disabled /></button>
            </form>
        </React.Fragment>
    )
}

export {SearchBar}