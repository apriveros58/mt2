import React, { useEffect, useState } from 'react'
import './statics/CSS/Filtros.css'
import * as tiendaServers from './tiendaServers'
import FilterAltIcon from '@mui/icons-material/FilterAlt';

const Filtros= () => {
    const [Materials, setMaterial] = useState([]);
    const listMaterial = async () => {
        try {
            const res = await tiendaServers.listMaterial();
            const data = await res.json();
            //console.log(data.Materials)
            setMaterial(data.Materials);
        } catch (error) {
            console.log(error);
        }
    };
    useEffect(() => {
        listMaterial();
    }, []);

    const [Hormas, setHorma] = useState([]);
    const listHormas = async () => {
        try {
            const res = await tiendaServers.listHormas();
            const data = await res.json();
            //console.log(data.Materials)
            setHorma(data.Hormas);
        } catch (error) {
            console.log(error);
        }
    };
    useEffect(() => {
        listHormas();
    }, []);

    return (
    <React.Fragment>
        <div className='Filtros'>
            <header>
                <table>
                    <tr>
                        <td><FilterAltIcon/></td>
                        <td><label><h3>Filtros</h3></label></td>
                        <td id="botones_filtros">
                            <input  type="button" value="Aplicar Filtros" disabled />
                        </td>
                        <td id="botones_filtros">
                            <input type="button" value="Quitar Filtros" disabled />
                        </td>
                    </tr>
                </table>
            </header>
            <form>
                <table>
                    <tr>
                        <td><label><h4>Modelo:</h4></label></td>
                        <td><label><h4>Diseño:</h4></label></td>
                    </tr>
                    <tr>
                        <td>
                        <ul>
                                {Hormas.map((Horma)=>(
                                    <li><input type="checkbox" name={Horma.name_h} disabled />{Horma.name_h}</li> 
                                ))}
                            </ul>
                        </td>
                        <td>
                            <ul>
                                {Materials.map((Material)=>(
                                    <li><input type="checkbox" name={Material.name_m} disabled />{Material.name_m}</li> 
                                ))}
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td><label><h4>Talla:</h4></label></td>
                        <td>
                            <ul>
                                <li>
                                    <input type="radio" value="No. específico" disabled />No. específico
                                    <input type="number" min="25" max="60" step="1" placeholder="Talla" disabled ></input>
                                </li>
                                <li>
                                    <input type="radio" value="Rango" disabled />Rango
                                    <input type="number" min="25" max="59" step="1" placeholder="Min" disabled />-
                                    <input type="number" min="26" max="60" step="1" placeholder="Max" disabled />
                                </li>
                            </ul>
                        </td>
                    </tr>
                </table>
                
                <ul>
                    <li>

                    </li>
                </ul>
            </form>
        </div>
    </React.Fragment>
    );
}

export {Filtros};