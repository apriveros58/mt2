import React, { useEffect, useState } from 'react'
/* Para importar los estilos CSS */
import './statics/CSS/GrillaProd.css'
/*Para organizar grilla */
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import * as tiendaServers from './tiendaServers'
import { Producto } from './Producto'

const GrillaProd = () => {
  //const navigate=useNavigate();

  const [Products, setProducts] = useState([]);  
    const listProducts = async () => {
        try {
            const res = await tiendaServers.listProducts();
            const data = await res.json();
            //console.log(data.Products)
            setProducts(data.Products);
        } catch (error) {
            console.log(error);
        }
    };
    useEffect(() => {
        listProducts();
    }, []);

  return (
    <React.Fragment>
      <div className='GrillaProd'>
        <Box sx={{ flexGrow: 1 }}>
          <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
            {Products.map((Product, index)=>(
                <Grid item xs={2} sm={4} md={4} key={index}>
                  <Producto key={Product.id} Product={Product}/>
                </Grid>
              ))}
          </Grid>
        </Box>
      </div>
  </React.Fragment>

  )

}
export {GrillaProd}

