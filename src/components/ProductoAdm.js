import React from 'react'
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';

import './statics/CSS/Producto.css'
import { useNavigate } from 'react-router-dom';


const ProductoAdm= ({Product}) =>{
    const navigate=useNavigate();
    return (
        <Card sx={{ maxWidth: 345 }} className="Producto" onClick={() => navigate(`/Edit/${Product.id}`)}>
            <CardMedia
                component="img"
                height="200"
                image={Product.image}
                alt={Product.name}
            />
            <CardHeader
                title={Product.name}
                subheader={Product.price}
            />
    </Card>
);
}

export {ProductoAdm};