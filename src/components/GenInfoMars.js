import React from 'react'
import '../components/statics/CSS/GenInfoMars.css'
import { GrillaTech } from './GrillaTech';
import { GrillaCharLoved } from './GrillaCharLoved';
import { GrillaCharHated } from './GrillaCharHated';
import { GrillaPartesBook } from './GrillaPartesBook';

function GenInfoMars() {
    return (
        <React.Fragment>
            <div className='Sectech'>
                <div id='Tech'>
                    <h2 >Tecnologías</h2>
                    <GrillaTech/>
                </div>
                <div id='Loved'>
                    <h2>Personajes Agradables</h2>
                    <GrillaCharLoved/>
                </div>
                <div id='Hated'>
                    <h2>Personajes NO agradables</h2>
                    <GrillaCharHated/>
                </div>
                <div id='Book'>
                    <h2>Partes del Libro</h2>
                    <GrillaPartesBook/>
                </div>
            </div>
        </React.Fragment>
    );
}

export {GenInfoMars};