import React from 'react'
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import { useNavigate } from 'react-router-dom';
import './statics/CSS/InfoCard.css'

const ParteBook= ({Tecno}) =>{
    const navigate=useNavigate();
    return (
        <div className='Tech'>
            <Card sx={{ maxWidth: 345 }} className="Producto" onClick={() => navigate(`/Infoparte/${ParteBook.id}`)}>
            <CardMedia
                component="img"
                height="200"
                image="https://9n52lh.deta.dev/download/mars.png"
                alt='Nombre'
            />
            <CardHeader
                title='Nombre'
            />
        </Card>
        </div>
    );
}
export {ParteBook};