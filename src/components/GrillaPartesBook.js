import React from 'react'
/* Para importar los estilos CSS */
import './statics/CSS/GrillaTech.css'
/*Para organizar grilla */
import { InfoCard } from './InfoCard'
import {
    Link
    } from "react-router-dom";

const GrillaPartesBook = () => {
    return (
        <React.Fragment>
            <div className='GrillaTech'>
                <Link to="/InfoParte/1">
                    <button>
                    <InfoCard
                        name="Disco"
                        img="https://img.freepik.com/vector-gratis/fondo-bola-discoteca_1284-5130.jpg?w=740&t=st=1670264125~exp=1670264725~hmac=4fc258e4ef036f125e5e3cc7d55af1d46aae1eae4a01eb8ff9f495c05aa1c028"/>
                    </button>
                </Link>
                <Link to="/InfoParte/2">
                    <button>
                    <InfoCard
                        name="Botánicos"
                        img="https://img.freepik.com/vector-gratis/conjunto-hojas_53876-91268.jpg?w=1060&t=st=1670264270~exp=1670264870~hmac=f30a9ee5fd26dc34a46058bb3e7ee7d15c95e8187b59d94b3a742349a1e2c0a5"/>
                    </button>
                </Link>
                <Link to="/InfoParte/3">
                    <button>
                    <InfoCard
                        name="Purificar"
                        img="https://cdn-icons-png.flaticon.com/512/1138/1138977.png?w=740&t=st=1670264389~exp=1670264989~hmac=786e94d457caf509f8d774ef76bb75ec235fcff0ca19f9fbbaf1755871c9d96e"/>
                    </button>
                </Link>
            </div>
        </React.Fragment>

    )

}
export {GrillaPartesBook}