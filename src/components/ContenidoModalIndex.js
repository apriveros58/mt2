import React, {useState, useEffect} from 'react'
import '../components/statics/CSS/ContenidoModal.css'
import IconButton from '@mui/material/IconButton';
import AddShoppingCart from '@mui/icons-material/AddShoppingCart';
import { useParams } from 'react-router-dom';
import * as tiendaServers from './tiendaServers'

function ContenidoModalIndex() {

    
    const params = useParams();

    const [Products, setProducts] = useState([]);  

    const getProducts = async (productId) => {
        try {
            const res = await tiendaServers.getProducts(productId);
            const data = await res.json();
            console.log(data);
            const { name, id_horma_id, id_material_id, image, size, price, stock, available} = data.Product;
            setProducts({ name, id_horma_id, id_material_id, image, size, price, stock, available});
        } catch (error) {
            console.log(error);
        }
        };
    
        useEffect(() => {
        if (params.id) {
            getProducts(params.id);
        }
        // eslint-disable-next-line
        }, []);

        //traer informacion de materiales
        const [Materials, setMaterial] = useState([]);
        const listMaterial = async () => {
            try {
                const res = await tiendaServers.listMaterial();
                const data = await res.json();
                //console.log(data.Materials)
                setMaterial(data.Materials);
            } catch (error) {
                console.log(error);
            }
        };
        useEffect(() => {
            listMaterial();
        }, []);

        //traer informacion de Hormas
        const [Hormas, setHorma] = useState([]);
        const listHormas = async () => {
            try {
                const res = await tiendaServers.listHormas();
                const data = await res.json();
                //console.log(data.Materials)
                setHorma(data.Hormas);
            } catch (error) {
                console.log(error);
            }
        };
        useEffect(() => {
            listHormas();
        }, []);

    return (
    <React.Fragment>
        <section className='ContenidoModal'>
                    <table className='izq'>
                        <tr>
                            <td><label for="Nombre" >Nombre: </label></td>
                            <td><label name="name">{Products.name}</label></td>
                        </tr>
                        <tr><td colspan="2"><img src={Products.image} alt="Logo"/></td></tr>
                    </table>
                    <table className='der'>
                        <tr>
                                <td><label for="Talla" >Talla: </label></td>
                                <td><label>{Products.size}</label></td>
                            </tr>
                            <tr>
                                <td><label for="Precio" >Precio: </label></td>
                                <td><label>$ {Products.price} COP</label></td>
                            </tr>
                            <tr>
                                <td><label for="Modelo" >Modelo: </label></td>
                                {Hormas.map((Horma)=>(
                                    Horma.id===Products.id_horma_id && 
                                        <td><label>{Horma.name_h}</label></td>
                                ))}
                            </tr>
                            <tr>
                                <td><label for="Diseño" >Diseño: </label></td>
                                {Materials.map((Material)=>(
                                        Material.id===Products.id_material_id && 
                                        <td><label>{Material.name_m}</label></td>
                                    ))}
                            </tr>
                            <tr>
                                <td><label for="Cantidad" >Cantidad: </label></td>
                                <td><label>{Products.stock}</label></td>
                            </tr>
                    </table>
                    <button className='Boton'>
                        <IconButton aria-label="Añadir al carrito">
                            <AddShoppingCart />
                        </IconButton>
                    </button>
        </section>
    </React.Fragment>
    );
}

export {ContenidoModalIndex};