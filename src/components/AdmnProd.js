import React, {useState, useEffect} from 'react'
import '../components/statics/CSS/ContenidoModal.css'
import { useNavigate, useParams } from 'react-router-dom';
import * as tiendaServers from './tiendaServers'

function AdmnProd(){

    const params = useParams();
    const navigate=useNavigate();
    //traer la informacion de los productos
    //generar el available true por defecto
    const initialState={
        available: "true"}
        const [Products, setProducts] = useState(initialState);  
    //traer la informacion de los productos
    const getProducts = async (productId) => {
        try {
            const res = await tiendaServers.getProducts(productId);
            const data = await res.json();
            console.log(data);
            const { name, id_horma_id, id_material_id, image, size, price, stock, available} = data.Product;
            setProducts({ name, id_horma_id, id_material_id, image, size, price, stock, available});
        } catch (error) {
            console.log(error);
        }
        };
    
        useEffect(() => {
        if (params.id) {
            getProducts(params.id);
        }
        // eslint-disable-next-line
        }, []);
    
        //funcion de detectar cada cambio sobre los inputs
        const handleInputChange = (e) => {
            setProducts({ ...Products, [e.target.name]: e.target.value });
        };

        //funcion PUT- modificar
        const handleSubmit = async (e) => {
            e.preventDefault();
            //console.log(Products);
            try {
                let res;
                res = await tiendaServers.updateProducts(params.id, Products);
                const data = await res.json();
                console.log(data)
                navigate("/Admin");
            } catch (error) {
                console.log(error);
            }
            
        };

        //funcion DELETE -borrar
        const handleDelete = async (e) => {
            e.preventDefault();
            try{
                await tiendaServers.deleteProducts(params.id);
                navigate("/Admin");
            } catch (error) {
                console.log(error);
            }
        };

        //traer informacion de materiales
        const [Materials, setMaterial] = useState([]);
        const listMaterial = async () => {
            try {
                const res = await tiendaServers.listMaterial();
                const data = await res.json();
                //console.log(data.Materials)
                setMaterial(data.Materials);
            } catch (error) {
                console.log(error);
            }
        };
        useEffect(() => {
            listMaterial();
        }, []);

        //traer informacion de Hormas
        const [Hormas, setHorma] = useState([]);
        const listHormas = async () => {
            try {
                const res = await tiendaServers.listHormas();
                const data = await res.json();
                //console.log(data.Materials)
                setHorma(data.Hormas);
            } catch (error) {
                console.log(error);
            }
        };
        useEffect(() => {
            listHormas();
        }, []);


    return (
        <React.Fragment>
            <form className='ContenidoModal'>
                <section className="izq">
                    <table>
                        <tr>
                            <td><label for="Nombre" >Nombre: </label></td>
                            <td><input name="name" type="text" value={Products.name} onChange={handleInputChange} placeholder="Nombre artículo" id="nom_art2"/></td>
                        </tr>
                        <tr>
                            <td><label for="Url" >Url: </label></td>
                            <td><input name="image" type="text" value={Products.image} onChange={handleInputChange} placeholder="Nombre artículo" id="nom_art2"/></td>
                        </tr>
                        <tr><td colspan="2"><img src={Products.image} alt="Subir imagen" id="img" value={Products.name}/></td></tr>
                        <tr>
                            <td><label for="Ref">Referencia: </label></td>
                            <td><label for="ref" id="ex_nom" value={Products.id}>{Products.id}</label></td>
                        </tr>
                    </table>
                </section>
                <section className="der">
                    <table>
                        <tr>
                            <td><label for="Cantidad">Cantidad: </label></td>
                            <td><input name="stock" type="number" min="1" max="2500000" step="1" placeholder="Cantidad" width="25" id="cant" value={Products.stock} onChange={handleInputChange}/></td>
                        </tr>
                        <tr>
                            <td><label for="Talla">Talla: </label></td>
                            <td><input name="size" type="number" min="25" max="60" step="1" placeholder="Talla" id="talla" value={Products.size} onChange={handleInputChange}/></td>
                        </tr>
                        <tr>
                            <td><label for="Precio">Precio:</label></td>
                            <td>
                                <input name="price" type="number" min="35000" max="1000000" placeholder="$ Precio COP" id="precio" value={Products.price} onChange={handleInputChange}/>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="Modelo">Modelo: </label></td>
                            <td>
                                <select name="id_horma_id" id="modelo" onChange={handleInputChange}>
                                    {Hormas.map((Horma)=>(
                                        Horma.id===Products.id_horma_id && 
                                        <option id="ordenar_item" selected disabled hidden>{Horma.name_h}</option>
                                    ))}
                                    {Hormas.map((Horma)=>(
                                        <option id="ordenar_item" value={Horma.id}>{Horma.name_h}</option>
                                    ))}
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="Diseño">Diseño: </label></td>
                            <td>
                                <select name="id_material_id" id="diseño" onChange={handleInputChange}>
                                {Materials.map((Material)=>(
                                        Material.id===Products.id_material_id && 
                                        <option id="ordenar_item" selected disabled hidden>{Material.name_m}</option>
                                    ))}
                                    {Materials.map((Material)=>(
                                        <option id="ordenar_item" value={Material.id}>{Material.name_m}</option>
                                    ))}
                                </select>
                            </td>
                        </tr>
                    </table>
                    <button class="botones_modal" id="main_button" onClick={handleSubmit}>Modificar</button>
                    <button class="botones_modal" id="main_button" onClick={handleDelete}>Eliminar</button>
                </section>
            </form>
        </React.Fragment>
    )
}

export {AdmnProd}